<?php get_header() ?>

<div class="container mx-auto my-10">

	<?php if ( have_posts() ) : ?>

        <?php while ( have_posts() ) : the_post() ?>

			<?php get_template_part( 'template-parts/card', get_post_format() ) ?>

		<?php endwhile ?>

        <?php the_posts_pagination() ?>

	<?php endif ?>

</div>

<?php get_footer() ?>
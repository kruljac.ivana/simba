/** @type {import('tailwindcss').Config} */

// -- Color pallete: https://coolors.co/palette/8ecae6-219ebc-023047-ffb703-fb8500

module.exports = {
    content: [
        './*.php',
        './**/*.php',
        './assets/build/css/*.css',
        './assets/build/js/*.js',
        './node_modules/flowbite/**/*.js'
    ],
    darkMode: 'class',
    theme: {
        container: {
            padding: {
                DEFAULT: '1rem',
            },
        },
        extend: {
            colors: {
                'primary': '#FB8500',
                'secondary': "#219EBC",
                'dark' : '#023047',
            },
        }
    },
    plugins: [
        require('flowbite/plugin'),
        require('@tailwindcss/typography')
    ]
};

<!DOCTYPE html>
<html <?php language_attributes() ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ) ?>">
	<meta name="viewport" content="width=device-width">
	<?php wp_head() ?>
</head>
<body class="antialiased">
	<div class="md:flex min-h-screen">
		<div class="w-full md:w-1/2 flex items-center justify-center">
			<div class="m-8">
				<div class="text-5xl md:text-9xl text-gray-800 border-primary border-b">404</div>
				<div class="w-16 h-1 bg-purple-light my-3 md:my-6"></div>
				<p class="text-gray-800 text-2xl md:text-3xl font-light mb-12"><?php _e( 'Sorry, the page you are looking for could not be found.', 'simba' ) ?></p>
				<a href="<?= site_url() ?>" class="bg-primary px-10 py-4 font-bold rounded text-white hover:bg-secondary">
					<?php _e( 'Go Home', 'simba' ) ?>
				</a>
			</div>
		</div>
	</div>
</body>
</html>

</main>

<?php do_action( 'simba_content_end' ) ?>

</div>

<?php do_action( 'simba_content_after' ) ?>

<footer class="dark:bg-slate-900 bg-white py-10 mt-6">
	<?php do_action( 'simba_footer' ) ?>

	<div class="container mx-auto text-center text-gray-500">
		&copy; <?= date_i18n( 'Y' ) ?> - <?= get_bloginfo( 'name' ) ?>
	</div>
</footer>

</div>

<?php wp_footer() ?>

</body>
</html>
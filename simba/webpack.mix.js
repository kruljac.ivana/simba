let mix = require('laravel-mix');
let path = require('path');

// mix.setResourceRoot('../');
mix.setPublicPath(path.resolve('./'));

mix.webpackConfig({
    watchOptions: { ignored: [
        path.posix.resolve(__dirname, './node_modules'),
        path.posix.resolve(__dirname, './css'),
        path.posix.resolve(__dirname, './js')
    ] }
});

mix.js('assets/build/js/app.js', 'js');

mix.postCss('assets/build/css/app.css', 'css');

mix.postCss('assets/build/css/editor-style.css', 'css');

mix.browserSync({
    proxy: 'http://localhost/simba',
    host: '',
    open: 'external',
    port: 1111
});

if (mix.inProduction()) {
    mix.version();
} else {
    Mix.manifest.refresh = _ => void 0
}

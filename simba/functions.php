<?php

/**
 * Theme setup.
 */
function simba_setup() {
	add_theme_support( 'title-tag' );

	register_nav_menus(
		array(
			'primary' => __( 'Primary Menu', 'simba' ),
		)
	);

	add_theme_support(
		'html5',
		array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		)
	);

    add_theme_support( 'custom-logo' );
	add_theme_support( 'post-thumbnails' );

	add_theme_support( 'align-wide' );
	add_theme_support( 'wp-block-styles' );

	add_theme_support( 'editor-styles' );
    add_editor_style( 'css/editor-style.css' );

    // -- minify HTML
    require_once('assets/config/html-compress.php');
}

add_action( 'after_setup_theme', 'simba_setup' );

/**
 * Enqueue theme assets.
 */
function simba_enqueue_scripts() {
	$theme = wp_get_theme();

	wp_enqueue_style( 'simba', simba_asset( 'css/app.css' ), array(), $theme->get( 'Version' ) );
	wp_enqueue_script( 'simba', simba_asset( 'js/app.js' ), array(), $theme->get( 'Version' ) );
}

add_action( 'wp_enqueue_scripts', 'simba_enqueue_scripts' );

/**
 * Get asset path.
 *
 * @param string  $path Path to asset.
 *
 * @return string
 */
function simba_asset( $path ) {
	if ( wp_get_environment_type() === 'production' ) {
		return get_stylesheet_directory_uri() . '/' . $path;
	}

	return add_query_arg( 'time', time(),  get_stylesheet_directory_uri() . '/' . $path );
}

/**
* Setting all .js on async defer
*/
if (!is_admin()) {
    function defer_js( $url ) {
        // -- exclude:
        // if (strpos($url, 'jquery.min.js')) return $url;
        return str_replace(' src', ' defer src', $url);
    }
    add_filter('script_loader_tag', 'defer_js', 10);
}

/**
* Remove unnecessary code from wp-header
*/
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('admin_print_scripts', 'print_emoji_detection_script');
remove_action('wp_print_styles', 'print_emoji_styles');
remove_action('admin_print_styles', 'print_emoji_styles');
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'wp_resource_hints', 2);
remove_action('wp_head', 'rest_output_link_wp_head', 10);

/**
* Deregister default WP scripts (we don't need it on frontend)
*/
function deregister_scripts() {
    if (!is_admin()) {
        // -- jquery.min.js
        wp_dequeue_script('jquery');
        wp_deregister_script('jquery');
        wp_register_script('jquery', false);
    }
}
add_action('wp_print_scripts', 'deregister_scripts', 100);

/**
* Remove jQuery migrate
*/
function remove_jquery_migrate($scripts) {
    if (!is_admin() && isset($scripts->registered['jquery'])) {
        $script = $scripts->registered['jquery'];
        if ($script->deps) { 
            // Check whether the script has any dependencies
            $script->deps = array_diff($script->deps, array('jquery-migrate'));
        }
    }
}
add_action('wp_default_scripts', 'remove_jquery_migrate');
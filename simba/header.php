<!DOCTYPE html>
<html <?php language_attributes() ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ) ?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="google" content="notranslate">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name=apple-mobile-web-app-capable content=yes>
<meta name=apple-mobile-web-app-status-bar-style content=yes>
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ) ?>">
<?php wp_head() ?>
<script>if (localStorage.getItem('color-theme') === 'dark' || (!('color-theme' in localStorage) && window.matchMedia('(prefers-color-scheme: dark)').matches)) {
    document.documentElement.classList.add('dark');
} else {
    document.documentElement.classList.remove('dark');
}</script>
</head>
<body <?php body_class( 'dark:bg-slate-800 dark:text-white bg-white text-dark antialiased' ) ?>>

<?php do_action( 'simba_site_before' ) ?>

<div id="page" class="min-h-screen flex flex-col">

	<?php do_action( 'simba_header' ) ?>

	<header class="dark:bg-slate-900 dark:text-white bg-white">
		<div class="mx-auto container">
            <?php get_template_part( 'template-parts/nav', 'web' ) ?>
            <?php get_template_part( 'template-parts/nav', 'mobile' ) ?>
		</div>
	</header>

    <?php get_template_part( 'modals/sign-in' ) ?>

	<div id="content" class="site-content flex-grow">

		<?php if ( is_front_page() && !is_paged() ) : ?>
            <div class="bg-white dark:bg-slate-900 dark:text-white pt-4 pb-20 mb-16">
                <div class="mx-auto container">
                    <div class="intro">
                        <div class="mx-auto max-w-screen-xl">
                            <h1 class="text-3xl lg:text-6xl max-w-screen-lg tracking-tight font-extrabold text-gray-800 dark:text-white mb-6">Start building your next <a href="https://tailwindcss.com" class="text-secondary">Tailwind CSS</a> flavoured WordPress theme  with <a href="<?= site_url() ?>/about/" class="text-primary">Simba</a>.</h1>
                            <p class="text-gray-600 dark:text-white text-xl font-medium mb-10">Simba is a developer starter WordPress theme with Tailwind CSS. <a href="<?= site_url() ?>/about/" class="text-secondary underline">Find out more about the theme »</a></p>
                            <a href="https://gitlab.com/kruljac.ivana/simba.git" rel="follow" class="inline-flex items-center text-md font-bold px-6 py-4 mr-3 font-bolder text-white bg-secondary border-gray-200 rounded-lg focus:outline-none hover:bg-primary">
                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M22.65 14.39L12 22.13 1.35 14.39a.84.84 0 0 1-.3-.94l1.22-3.78 2.44-7.51A.42.42 0 0 1 4.82 2a.43.43 0 0 1 .58 0 .42.42 0 0 1 .11.18l2.44 7.49h8.1l2.44-7.51A.42.42 0 0 1 18.6 2a.43.43 0 0 1 .58 0 .42.42 0 0 1 .11.18l2.44 7.51L23 13.45a.84.84 0 0 1-.35.94z"/></svg>
                                <span class="ml-2">Download from GitLab</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
		<?php endif ?>

		<?php do_action( 'simba_content_start' ) ?>

		<main>
<!-- Web nav: -->
<nav class="relative py-4 flex justify-between items-center">
    <?php if ( has_custom_logo() ) : 
        the_custom_logo();
        else : ?>
            <a class="text-3xl font-bold" href="<?= site_url() ?>">
                <span class="font-extrabold text-lg uppercase"><?= get_bloginfo( 'name' ) ?></span>
            </a>
    <?php endif ?>
    <?php wp_nav_menu(
        array(
            'container_id'    => 'primary-menu',
            'container_class' => 'web-menu hidden lg:flex',
            'menu_class'      => 'absolute top-1/2 left-[40%] xl:left-1/2 transform -translate-y-1/2 -translate-x-1/2 flex lg:mx-auto lg:items-center lg:w-auto lg:space-x-8',
            'theme_location'  => 'primary',
            'li_class'        => 'lg:mx-4',
            'fallback_cb'     => false,
        )) ?>
    <button id="theme-toggle" class="lg:inline-flex ml-auto lg:mr-3 text-gray-500 dark:text-gray-400 hover:bg-gray-100 dark:hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-gray-200 dark:focus:ring-gray-700 rounded-lg text-sm p-2">
        <svg id="theme-toggle-dark-icon" class="w-5 h-5 hidden" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="M17.293 13.293A8 8 0 016.707 2.707a8.001 8.001 0 1010.586 10.586z"></path></svg>
        <svg id="theme-toggle-light-icon" class="w-5 h-5 hidden" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="M10 2a1 1 0 011 1v1a1 1 0 11-2 0V3a1 1 0 011-1zm4 8a4 4 0 11-8 0 4 4 0 018 0zm-.464 4.95l.707.707a1 1 0 001.414-1.414l-.707-.707a1 1 0 00-1.414 1.414zm2.12-10.607a1 1 0 010 1.414l-.706.707a1 1 0 11-1.414-1.414l.707-.707a1 1 0 011.414 0zM17 11a1 1 0 100-2h-1a1 1 0 100 2h1zm-7 4a1 1 0 011 1v1a1 1 0 11-2 0v-1a1 1 0 011-1zM5.05 6.464A1 1 0 106.465 5.05l-.708-.707a1 1 0 00-1.414 1.414l.707.707zm1.414 8.486l-.707.707a1 1 0 01-1.414-1.414l.707-.707a1 1 0 011.414 1.414zM4 11a1 1 0 100-2H3a1 1 0 000 2h1z" fill-rule="evenodd" clip-rule="evenodd"></path></svg>
    </button>
    <div class="lg:hidden">
        <button class="navbar-burger flex items-center dark:text-white text-dark p-3">
            <svg class="block h-5 w-5 fill-current" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Mobile menu</title><path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"></path></svg>
        </button>
    </div>
    <button data-modal-target="signIn" data-modal-toggle="signIn" class="hidden lg:inline-flex lg:mr-3 py-2 px-6 border border-gray-300 hover:bg-secondary hover:text-white hover:border-secondary text-sm font-bold rounded-md transition duration-200">Sign In</button>
    <a class="hidden lg:inline-flex items-center py-2 px-4 border border-primary bg-primary hover:bg-secondary hover:border-secondary text-sm text-white font-bold rounded-md transition duration-200" href="https://gitlab.com/kruljac.ivana/simba.git">
        <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M22.65 14.39L12 22.13 1.35 14.39a.84.84 0 0 1-.3-.94l1.22-3.78 2.44-7.51A.42.42 0 0 1 4.82 2a.43.43 0 0 1 .58 0 .42.42 0 0 1 .11.18l2.44 7.49h8.1l2.44-7.51A.42.42 0 0 1 18.6 2a.43.43 0 0 1 .58 0 .42.42 0 0 1 .11.18l2.44 7.51L23 13.45a.84.84 0 0 1-.35.94z"/></svg>
        <span class="ml-2">GitLab</span>
    </a>
</nav>
<article id="post-<?php the_ID() ?>" <?php post_class( 'entry-content max-w-screen-lg mx-auto mb-12' ) ?>>
	
    <div class="mb-5">
        <h1 class="text-2xl md:text-5xl font-extrabold leading-tight mb-1"><?= the_title() ?></h1>
		<time datetime="<?= get_the_date( 'c' ) ?>" itemprop="datePublished" class="text-sm text-gray-700 dark:text-gray-500"><?= get_the_date() ?></time>
    </div>

    <?php // -- you can get caption and description:
    $img_caption = get_post(get_post_thumbnail_id()) -> post_excerpt;
    $img_description = get_post(get_post_thumbnail_id()) -> post_content;
    if (has_post_thumbnail() ) : ?>
    <figure class="featured">
        <?php the_post_thumbnail('large') ?>
    </figure>
    <div><?= $img_caption ?></div>
    <?php endif ?>

	<div class="prose dark:prose-invert lg:prose-lg max-w-screen-lg">
		<?php the_content() ?>

		<?php wp_link_pages(
				array(
					'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'simba' ) . '</span>',
					'after'       => '</div>',
					'link_before' => '<span>',
					'link_after'  => '</span>',
					'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'simba' ) . ' </span>%',
					'separator'   => '<span class="screen-reader-text">, </span>',
				)
			) ?>
	</div>

</article>
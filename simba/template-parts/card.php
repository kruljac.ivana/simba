<div class="card">
    <div class="featured" style="background-image: url('<?php bloginfo('template_directory') ?>/assets/img/placeholder.jpg')">
        <?php if (has_post_thumbnail()): the_post_thumbnail('medium'); endif ?>
    </div>
    <div class="px-5 py-4 flex flex-col justify-between leading-normal">
        <div class="mb-4">
            <h2 class="text-2xl md:text-3xl font-extrabold leading-tight mb-1"><a class="hover:underline" href="<?= get_the_permalink() ?>"><?= the_title() ?></a></h2>
            <time datetime="<?= get_the_date( 'c' ) ?>" itemprop="datePublished" class="text-sm text-gray-700 dark:text-gray-500"><?= get_the_date() ?></time>
            <div class="line-clamp-3 mt-2"><?= mb_strimwidth(get_the_excerpt(), 0, 300) ?></div>
            <div class="text-right mt-4 mr-4">
                <a class="text-secondary font-bold underline hover:text-primary" href="<?= get_the_permalink() ?>"><?= __( 'Read more', 'simba' ) ?></a>
            </div>
        </div>
    </div>
</div>
<!-- Mobile nav: -->
<div class="navbar-menu relative z-50 hidden">
    <div class="navbar-backdrop fixed inset-0 bg-gray-800 dark:opacity-90 opacity-50"></div>
    <nav class="fixed top-0 left-0 bottom-0 flex flex-col w-5/6 max-w-sm py-6 px-6 bg-white dark:bg-gray-700 overflow-y-auto">
        <div class="flex justify-between items-center mb-8">
            <?php if ( has_custom_logo() ) :
                the_custom_logo();
                else : ?>
                    <a class="text-3xl font-bold" href="<?= site_url() ?>">
                        <span class="font-extrabold text-lg uppercase"><?= get_bloginfo( 'name' ) ?></span>
                    </a>
            <?php endif ?>
            <button class="navbar-close">
                <svg class="h-6 w-6 text-gray-500 dark:text-white cursor-pointer hover:text-gray-700" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12"></path>
                </svg>
            </button>
        </div>
        <?php wp_nav_menu(
            array(
                'container_id'    => 'primary-mobile-menu',
                'container_class' => '',
                'menu_class'      => 'mobile-menu',
                'theme_location'  => 'primary',
                'li_class'        => 'mb-1',
                'fallback_cb'     => false,
            )) ?>
        <div class="mt-auto">
            <div class="pt-6">
                <button data-modal-target="signIn" data-modal-toggle="signIn" class="w-full px-4 py-3 mb-3 leading-loose text-center font-semibold border border-gray-300 hover:bg-secondary hover:text-white dark:hover:border-secondary rounded-lg">Sign in</button>
                <a class="block px-4 py-3 mb-2 leading-loose text-center text-white font-semibold bg-secondary hover:bg-primary rounded-lg" href="https://gitlab.com/kruljac.ivana/simba.git">Download from GitLab</a>
            </div>
            <p class="my-4 text-xs text-center text-gray-400">
                <span>&copy; <?= date_i18n( 'Y' ) ?> - <?= get_bloginfo( 'name' ) ?></span>
            </p>
        </div>
    </nav>
</div>
<p><img src="simba/assets/img/simba.png" height="60" alt="Simba"></p>

# About

Simba is a starter developer theme for WordPress using [Tailwind CSS](https://tailwindcss.com/), Webpack (Laravel Mix) and some other cool stuff. You can read more about it here - [Simba theme settings](https://simba-theme.netlify.app/about/).

## Development

* Navigate into your WP themes folder and clone repo: `git clone https://gitlab.com/kruljac.ivana/simba.git && cd simba`
* Run `npm install`
* Run `npm run watch` to start developing

You will find the editable CSS and Javascript files within the `/assets/build/css` and `/assets/build/js` folder.

Before you use your theme in production, make sure you run `npm run production`.

### NPM Scripts

 NPM scripts are available in the `package.json` file under "scripts". A script is executed through the terminal by running `npm run script-name`.

| Script     | Description                                                                    |
|------------|--------------------------------------------------------------------------------|
| production | Creates a production (minified) build of app.js, app.css and editor-style.css. |
| dev        | Creates a development build of app.js, app.css and editor-style.css.           |
| watch      | Runs several watch scripts concurrently.                                       |

## Theme colors (& defaults)

Main colors and other theme defaults can be modified in `tailwind.config.js`.

Here you can check out used [Color palette](https://coolors.co/palette/8ecae6-219ebc-023047-ffb703-fb8500).

## Block editor support

Simba comes with support for the [block editor](https://wordpress.org/support/article/wordpress-editor/).

To make the editing experience within the block editor more in line with the front end styling, a `editor-style.css` is generated.

## Additional links

* [Tailwind CSS Documentation](https://tailwindcss.com/docs)
* [Laravel Mix Documentation](https://laravel-mix.com)

## Author

[Ivana Kruljac](https://ivanakruljac.netlify.app/)

#### Simba Dark
<img src="simba/assets/img/simba-dark.png" style="width:100%;height:auto" alt="Simba Dark" />

#### Simba Light
<img src="simba/assets/img/simba-light.png" style="width:100%;height:auto" alt="Simba Light" />